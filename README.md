# Overview

This repository is meant to store the topology and CI for automatically building the most recent CITC image when a new version of CL becomes available.

This repository primarily exists as a `topology.dot` file and a series of scripts that automatically communicate with the AIR api (air.nvidia.com/api) to create a new snapshot for CITC when a new version of CL is released.